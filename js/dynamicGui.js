function logAtk(user, move) {
    let msg = user + " utilise " + move + ".";
    console.log(msg);
    displayMessage(msg);
}

function animTackle() {
    SVG('#eevee')
        .animate().move(150, 100)
        .animate().move(30, 200)
        .delay(200).after(x => onEeveeMoveFinished());
}

function animGrowl() {
    // show waves
    let draw = SVG('svg');
    var growl = draw.use('growl').move(60,-5);
    // double la longueur
    growl.animate().scale(2.5)
        .animate().scale(0.5)
        .animate().scale(2.5)
        .animate().scale(0.5)
        .animate().opacity(0)
        .delay(200).after(x => onEeveeMoveFinished());
}

function animMeteore() {
    // show meteores
    let draw = SVG('svg');
    var etoile1 = draw.use('meteore').move(40,-45);
    var etoile2 = draw.use('meteore').move(70,-80);
    var etoile3 = draw.use('meteore').move(110,-50);
    // move meteore
    etoile1.animate().move(240,-195);
    etoile2.animate().move(270,-230);
    etoile3.animate().move(310,-200);
    // add some rotation
    etoile1.animate(2000).rotate(360)
            .animate().move(340,-305);
    etoile2.animate(2000).rotate(360)
            .animate().move(370,-340);
    etoile3.animate(2000).rotate(360)
            .animate().move(410,-310)
            .delay(200).after(x => onEeveeMoveFinished());
}

function animBite() {

    // Affichage des dents
    let draw = SVG('svg');
    var haut = draw.use('dent_haut').move(280,-180);
    var bas = draw.use('dent_bas').move(280,-160);
    let x = 280;
    // Rapprochement des dents
    haut.animate().move(x,-165)
        .animate().move(x,-180)
        .animate().move(x,-165)
        .animate().move(x,-180)
        .animate().opacity(0);
    bas.animate().move(x,-175)
        .animate().move(x,-160)
        .animate().move(x,-175)
        .animate().move(x,-160)
        .animate().opacity(0)
        .delay(200).after(x => onEeveeMoveFinished());
}

function animSplash(){
    let x = 330;
    let y1 = 0;
    let y2 = 20;
    let d = 300; // duration
    SVG('#magikarp')
        .animate(d).move(x, y1).animate(d).move(x, y2)
        .animate(d).move(x, y1).animate(d).move(x, y2)
        .animate(d).move(x, y1).animate(d).move(x, y2)
        .delay(200).after(x => onMagikarpMoveFinished());
}


function displayMessage(msg) {
    document.getElementById("msg").innerHTML = msg;
}


function updateHpBar() {
    SVG('#hp_bar')
        .attr({ "fill": hpColor() })
        .animate().width(1.3 * (magikarp_hp ));
}

function hpColor() {

    greenRatio = magikarp_hp / 100;
    redRatio = 1 - greenRatio;
    
    function channel(ratio) {
        ratio = Math.round(255 * ratio);
        var s = ratio.toString(16);
        return (s.length == 1) ? '0' + s : s;
    }
    return '#' + channel(redRatio) + channel(greenRatio) + "00";
}

function animDeath() {
    SVG('#magikarp').animate(3000).attr({ "opacity": 0 });
}

function eeveeTurnAround(){
    
    /* Version hack
    J'ai fixer le bug d'evolution avec un hack. puis en faisant le nettoyage du projet.
    J'ai trouver que c'etait causer par le fichier Evolie.png qui etait dans le dossier
    racine du projet au lieu de dans img/ .donc j'ai remis l'animation sans hack mais
    j'ai laisser le hack en commentaire. Ne demandez-moi pas pourquoi le .png causait
    probleme, j'en ai aucune idee... (WTF)
        Anthony    */
    // SVG("#eevee").delay().animate(1500).attr({"opacity":"0"});
    // SVG("#eevee2").delay(2000).animate(1500).attr({"opacity":"1"}).move(30,200).delay(500)
    //     .after(x => animEvolution());
    
    SVG("#eevee").delay(2000).animate(1500).attr({"opacity":"0"});
    SVG("#eevee2").delay(2000).animate(1500).attr({"opacity":"1"}).move(30,200).delay(500)
        .after(x => animEvolution());
}

function animEvolution(){
    playEvolutionTheme();
    displayMessage("Evoli is evolving !");
    // Evolve
    SVG("#eevee2") //               1.0
        .animate().attr({"opacity":"0.8"})
        .animate().attr({"opacity":"0.9"})
        .animate().attr({"opacity":"0.7"})
        .animate().attr({"opacity":"0.8"})
        .animate().attr({"opacity":"0.6"})
        .animate().attr({"opacity":"0.7"})
        .animate().attr({"opacity":"0.5"})
        .animate().attr({"opacity":"0.6"})
        .animate().attr({"opacity":"0.4"})
        .animate().attr({"opacity":"0"});
    SVG("#givrali") //              0.0
        .animate().attr({"opacity":"0.2"})
        .animate().attr({"opacity":"0.1"})
        .animate().attr({"opacity":"0.3"})
        .animate().attr({"opacity":"0.2"})
        .animate().attr({"opacity":"0.4"})
        .animate().attr({"opacity":"0.3"})
        .animate().attr({"opacity":"0.5"})
        .animate().attr({"opacity":"0.4"})
        .animate().attr({"opacity":"0.6"})
        .animate().attr({"opacity":"1.0"})
        .after(x => evolutionFinished());
}

function evolutionFinished(){
    displayMessage("Evoli evolved in Givrali !!!");
    playSound("Glaceon");
}

// Audio

function playSound(name) {
    let file = "audio/" + name + ".mp3";
    let audio = new Audio(file);
    audio.loop = false;
    audio.play();
}

function playBattleTheme() {
    window.music.play();
}

function playEvolutionTheme(){
    window.music.pause();
    window.music = new Audio("audio/Evolution.mp3").play();
}

// Allow autoplay audio
// Firefox : https://support.mozilla.org/en-US/kb/block-autoplay#w_site-settings
