
function onEeveeMoveFinished() {
    onMagikarpTurn();
}

function useSplash() {
    logAtk("Magikarp", "Splash");
    playSound("Splash");
    animSplash();
}

function onMagikarpMoveFinished() {
    enableControls();
}

function processEevee(move) {
    logAtk("Eevee", move);
    switch (move) {
        case CHARGE:        useTackle();        break;
        case GROWL:         useGrowl();         break;
        case METEORE:       useMeteor();        break;
        case MORSURE:       useBite();          break;
        default: break;
    }
}

function useTackle() {
    updateCursor(0, 0);
    playSound("Tackle");
    animTackle();
    magikarpLoseHp(20);
}

function useGrowl() {
    updateCursor(1, 0);
    playSound("Growl");
    animGrowl();
    increaseGrowlPower();
}

function useMeteor() {
    updateCursor(0, 1);
    playSound("Meteor");
    animMeteore();
    magikarpLoseHp(30);
}

function useBite() {
    updateCursor(1, 1);
    playSound("Bite");
    animBite();
    magikarpLoseHp(40);
}

// Hit Points 

function increaseGrowlPower() {
    window.growl_power *= 1.5;
}

function magikarpLoseHp(damage) {
    window.magikarp_hp -= damage * window.growl_power;
    //console.log("Il reste " + window.magikarp_hp + " HP a Magikarp");
    updateHpBar();

    if (isMagikarpHasNoHp())
        magikarpDie();
}

function isMagikarpHasNoHp() {
    return window.magikarp_hp <= 0;
}

function isMagikarpAlive() {
    return !isMagikarpHasNoHp();
}

function magikarpDie() {
    displayMessage("Magikarp fainted");
    playSound("Magikarp");
    postBattle();
}

function postBattle(){
    disableControls();
    animDeath();
    eeveeTurnAround();
}
