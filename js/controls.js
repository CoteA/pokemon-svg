
function onSelect(move) {
    tryToMove(move);
}

function onSpaceDown() {
    if(canUseMove()){
        disableControls();
        processEevee(calcMove());
    }
}

function tryToMove(move = null){
    
    if (move == null)
        processEevee(calcMove()); // Hit Space Key
    else
        processEevee(move); // Click
        
}

function canUseMove(){
    return isReady && isMagikarpAlive();
}

function disableControls(){
    isReady = false;
    SVG("#cursor").attr({"opacity":"0.5"});
}

function enableControls(){
    isReady = true;
    SVG("#cursor").attr({"opacity":"1"});
}

function onMagikarpTurn(){

    if(isMagikarpAlive())
        useSplash();
}

function updateCursor(h, v) {
    hPosition = h;
    vPosition = v;
    moveCursor();
}

function onKeyDown(e) {

    if( ! isSvgStarted){
        startSvgAnimation();
    }
    
    // event.keycode
    var keycode = (e.keyCode ? e.keyCode : e.which);
    interpretKeyDown(keycode);
    
    moveCursor(); // Update
}

function startSvgAnimation(){
    isSvgStarted = true;
    boyMeetGirl();
}

function interpretKeyDown(keycode){

    console.log(keycode);
    
    switch (keycode) {
        case LEFT:      hPosition = 0;          break;
        case RIGHT:     hPosition = 1;          break;
        case UP:        vPosition = 0;          break;
        case DOWN:      vPosition = 1;          break;
        case SPACE:     onSpaceDown();          break;
        case B_KEY:     transitionToBattle(0);  break;
        case K_KEY:     cheatKillMagikarp();    break;
        default:        break;
    }
}

function cheatKillMagikarp(){
    transitionToBattle(0);
    SVG("#battle").attr({"opacity":"1"});
    magikarpDie();
}

function moveCursor() {
    newX = 300 + hPosition * 110;
    newY = 410 + vPosition * 40;
    SVG('#cursor').move(newX, newY);
}

function calcMove() {
    fullPosition = (vPosition * 2) + hPosition;

    switch (fullPosition) {
        case 0:     return MOVE1;
        case 1:     return MOVE2;
        case 2:     return MOVE3;
        case 3:     return MOVE4;
        default:    break;
    }
}
