// Obsolete
function getDayPeriod() { // Pour l'evolution
    let hour = new Date().getHours();

    switch (hour) {
        case 23:
        case 0:
        case 1:
            return "Midnight";
        case 11:
        case 12:
        case 13:
            return "Noon";
        default:
            return "useless"; // N'affecte pas l'evolution
    }
}

function getSeason() {
    
    switch (new Date().getMonth()) {
        case 11:// Dec
        case 0: // Jan
        case 1: // Fevr
            return "Winter";
        case 2: // Mars
        case 3: // Avr
        case 4: // Mai
            return "Spring";
        case 5: // Juin
        case 6: // Juil
        case 7: // Aout
            return "Summer";
        case 8: // Sept
        case 9: // Oct
        case 10:// Nov
            return "Automn";
        default:
            return "Winter";
    }
}

function calcGrassColor(season) {
    switch (getSeason()) {
        case "Winter":
            return "white";
        case "Spring":
            return "green";
        case "Summer":
            return "limegreen";
        case "Automn":
            return "orange";
    }
}

function updateGrassColor() {
    //SVG('#bg').fill(calcGrassColor());
    SVG('#bg_light').attr({"lighting-color":calcGrassColor()});
}

function isDaylight() { // Pour jour/nuit
    let hour = new Date().getHours();

    return (6 <= hour) && (hour <= 18); // Entre 6h et 18h
}

function showNight() {
    if ( ! isDaylight()){
        SVG("#night").attr({"opacity":"0.7"});
        let color = "#344";
        SVG("#bg_msg").attr({"fill":color});
        SVG("#bg_menu").attr({"fill":color});
    }
}

// Obsolete
function calcEvolution() {
    let dayPeriod = getDayPeriod();

    if (dayPeriod == "Midnight") {
        return "Umbreon";
    } else if (dayPeriod == "Noon") {
        return "Sylveon";
    } else {
        switch (getSeason()) {
            case "Winter":
                return "Glaceon";
            case "Spring":
                return "Leafeon";
            case "Summer":
                return "Flameon";
            case "Automn":
                return "Vaporeon";
        }
    }
}

function logEvolution() {
    console.log("Eevee va bientot evoluer en " + calcEvolution());
}

logEvolution();
updateGrassColor();
showNight();