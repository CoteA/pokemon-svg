
function createTree(x, y){
    let draw = SVG("#forest");
    let tree = SVG("#tree");
    draw.use(tree).move(x, y);
}

function rand(min, max){
    return Math.floor((Math.random() * (max - min)) + min);
}

function randomize(max){
    return Math.floor((Math.random() + Math.random() ) * max);
}

function populateForest(){
    populateBack();
    populatePokecenter();
    populateField();
}

function populateBack(){
    populateRange(130, 550, -100, 0);
}

function populatePokecenter(){
    populateRange(-50, 85, 200, 600);
}

function populateField(){
    populateRange(180, 550, 100, 500);
}

function populateRange(x1, x2, y1, y2){
    
    // density
    const TREE_DENSITY = 4000;
    let delta_x = x2 - x1;
    let rarity_per_line = TREE_DENSITY / delta_x;

    // place tree du haut vers le bas (pour le bon z-index)
    for (let y = y1; y < y2; y += randomize(rarity_per_line)) {
        createTree(rand(x1, x2), y);
    }
}

populateForest();