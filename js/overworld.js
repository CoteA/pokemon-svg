
function boyMeetGirl(){
    SVG("#player_avatar")
        .animate(3000).move(150, 200).after(x => transitionToBattle());
    SVG("#trainer_avatar").delay(1000)
        .animate(1000).move(150, 150).after(x => playBattleTheme());
}

function transitionToBattle(speedup = 1){ // speedup is used for the cheat to skip to battle
        
    SVG("#excl")
        .attr({ "visibility": "true" })
        .animate(2000 * speedup)
        .size(500)
        .move(0,0);
    
    SVG("#overworld")
        .animate(3000 * speedup).attr({ "opacity": "0" })
        .after(x => showBattle());
}

function showBattle(){
    SVG("#battle").animate(2000).attr({ "opacity": "1" });
    SVG("#rough_ground").attr({ "width":"100%" }).attr({ "height":"100%" });
    SVG("#night").attr({"height":"400"});
    isReady = true;
    playSound("Magikarp");
}
