console.log("Started");
// Donnees globales a l'animation

// Keys
const SPACE = 32;
const LEFT  = 37;
const UP    = 38;
const RIGHT = 39;
const DOWN  = 40;
const B_KEY = 66; // Skip to Battle
const K_KEY = 75; // Kill Magikarp

// MOVES
const CHARGE  = "Charge";
const GROWL   = "Growl"; // Rugissement
const METEORE = "Météore";
const MORSURE = "Morsure";

const MOVE1 = CHARGE;
const MOVE2 = GROWL;
const MOVE3 = METEORE;
const MOVE4 = MORSURE;

// Resources
var music = new Audio("audio/Battle.mp3");

// Game State
var isSvgStarted = false;
var isReady      = false; // Ready to attack
var magikarp_hp  = 100;
var growl_power  = 1.0;

// Cursor
var hPosition = 0;
var vPosition = 0;
